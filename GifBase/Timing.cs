using System.IO;
using System;
using System.Diagnostics;

namespace Gif.Components
{
    public class ImageReference {
        public int Width;
        public int Height;
        public byte[] Pixels;
        public ImageReference(int w, int h) {
            Width = w;
            Height = h;
            Pixels = new byte[Width * Height * 3];
            Random r = new Random();
            r.NextBytes(Pixels);
        }

        public byte[] GetPixels ()
        {
            return Pixels;
        }
    }
    public static class Timer
    {
        public static double TimeIt(int w, int h, int frames) 
        {
            ImageReference aFrame = new ImageReference(w, h);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            var encoder = new AnimatedGifEncoder ();
            encoder.SetRepeat (0);
            encoder.SetFrameRate (15);
            encoder.SetQuality (12);
            encoder.Start (Path.GetTempFileName());

            // Grab the first frame so we can set the size of the
            // target JiffyGIF
            encoder.SetSize (aFrame.Width, aFrame.Height);
            for(var f = 0; f < frames; ++f)
                encoder.AddFrame (aFrame);
            encoder.Finish ();
            sw.Stop();

            return sw.Elapsed.TotalMilliseconds;
        }
    }
}
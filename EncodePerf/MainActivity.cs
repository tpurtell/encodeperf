﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;

namespace EncodePerf
{
    [Activity (Label = "EncodePerf", MainLauncher = true)]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button> (Resource.Id.myButton);
            
            button.Click += delegate {
                button.Text = string.Format ("{0} clicks!", count++);
            };
            button.Text = "running test already... wait";
            Gif.Components.Timer.TimeIt(128, 128, 1);
            Gif.Components2.Timer.TimeIt(128, 128, 1);
            Task.Run(() => {
                var t = Gif.Components.Timer.TimeIt(540, 540, 10);
                var t2 = Gif.Components.Timer.TimeIt(540, 540, 10);
                RunOnUiThread(() => {
                    button.Text = string.Format("untuned {0} ms, tuned {1} ms", t, t2);
                });
            });
        }
    }
}



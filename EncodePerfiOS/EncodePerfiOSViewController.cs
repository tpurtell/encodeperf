﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;

namespace EncodePerfiOS
{
    public partial class EncodePerfiOSViewController : UIViewController
    {
        public EncodePerfiOSViewController (IntPtr handle) : base (handle)
        {
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();
            
            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            
            // Perform any additional setup after loading the view, typically from a nib.
            Gif.Components.Timer.TimeIt(128, 128, 1);
            Gif.Components2.Timer.TimeIt(128, 128, 1);
            aLabel.Text = "Running test alreay... wait";
            Task.Run(() => {
                var t = Gif.Components.Timer.TimeIt(540, 540, 10);
                var t2 = Gif.Components.Timer.TimeIt(540, 540, 10);
                InvokeOnMainThread(() => {
                    aLabel.Text = string.Format("untuned {0} ms, tuned {1} ms", t, t2);
                });
            });        
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear (animated);
        }

        #endregion
    }
}


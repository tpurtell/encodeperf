// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace EncodePerfiOS
{
	[Register ("EncodePerfiOSViewController")]
	partial class EncodePerfiOSViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel aLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (aLabel != null) {
				aLabel.Dispose ();
				aLabel = null;
			}
		}
	}
}

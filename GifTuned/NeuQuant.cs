#region .NET Disclaimer/Info

//===============================================================================
//
// gOODiDEA, uland.com
//===============================================================================
//
// $Header :        $
// $Author :        $
// $Date   :        $
// $Revision:       $
// $History:        $
//
//===============================================================================

#endregion .NET Disclaimer/Info

#region Java

/* NeuQuant Neural-Net Quantization Algorithm
 * ------------------------------------------
 *
 * Copyright (c) 1994 Anthony Dekker
 *
 * NEUQUANT Neural-Net quantization algorithm by Anthony Dekker, 1994.
 * See "Kohonen neural networks for optimal colour quantization"
 * in "Network: Computation in Neural Systems" Vol. 5 (1994) pp 351-367.
 * for a discussion of the algorithm.
 *
 * Any party obtaining a copy of these files from the author, directly or
 * indirectly, is granted, free of charge, a full and unrestricted irrevocable,
 * world-wide, paid up, royalty-free, nonexclusive right and license to deal
 * in this software and documentation files (the "Software"), including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons who receive
 * copies from any such party to do so, with the only requirement being
 * that this copyright notice remain intact.
 */

// Ported to Java 12/00 K Weiner

#endregion Java

using System;
using System.Runtime.CompilerServices;

namespace Gif.Components2
{
    public unsafe class NeuQuant
    {
        protected const int netsize = 256; /*         number of colours used */
        /* four primes near 500 - assume no image has a length so large */
        /*         that it is divisible by all four primes */
        protected const int prime1 = 499;
        protected const int prime2 = 491;
        protected const int prime3 = 487;
        protected const int prime4 = 503;
        protected const int minpicturebytes = (3 * prime4);
        /*         minimum size for input image */
        /* Program Skeleton
        ----------------
        [select samplefac in range 1..30]
        [read image from input file]
        pic = (unsigned char*) malloc(3*width*height);
        initnet(pic,3*width*height,samplefac);
        learn();
        unbiasnet();
        [write output image header, using writecolourmap(f)]
        inxbuild();
        write output image using inxsearch(b,g,r)      */

            /* Network Definitions
            ------------------- */
        protected const int maxnetpos = (netsize - 1);
        protected const int netbiasshift = 4; /*         bias for colour values */
        protected const int ncycles = 100; /*         no. of learning cycles */

        /* defs for freq and bias */
        protected const int intbiasshift = 16; /*         bias for fractions */
        protected const int intbias = (((int)1) << intbiasshift);
        protected const int gammashift = 10; /*         gamma = 1024 */
        protected const int gamma = (((int)1) << gammashift);
        protected const int betashift = 10;
        protected const int beta = (intbias >> betashift); /*         beta = 1/1024 */
        protected const int betagamma =
            (intbias << (gammashift - betashift));

        /*         defs for decreasing radius factor */
        protected const int initrad = (netsize >> 3); /*         for 256 cols, radius starts */
        protected const int radiusbiasshift = 6; /*         at 32.0 biased by 6 bits */
        protected const int radiusbias = (((int)1) << radiusbiasshift);
        protected const int initradius = (initrad * radiusbias); /*         and decreases by a */
        protected const int radiusdec = 30; /*         factor of 1/30 each cycle */

        /* defs for decreasing alpha factor */
        protected const int alphabiasshift = 10; /*         alpha starts at 1.0 */
        protected const int initalpha = (((int)1) << alphabiasshift);

        protected int alphadec; /*         biased by 10 bits */

        /* radbias and alpharadbias used for radpower calculation */
        protected const int radbiasshift = 8;
        protected const int radbias = (((int)1) << radbiasshift);
        protected const int alpharadbshift = (alphabiasshift + radbiasshift);
        protected const int alpharadbias = (((int)1) << alpharadbshift);

        /*         Types and Global Variables
        -------------------------- */

        protected byte[] thepicture; /*         the input image itself */
        protected int lengthcount; /*         lengthcount = H*W*3 */

        protected int samplefac; /*         sampling factor 1..30 */


        public unsafe struct NqData
        {
            //   typedef int pixel[4];                /* BGRc */
            public fixed int network[netsize * 4]; /*         the network itself - [netsize][4] */

            public fixed int netindex[256];
            /*         for network lookup - really 256 */

            public fixed int bias[netsize];
            /*         bias and freq arrays for learning */
            public fixed int freq[netsize];
            public fixed int radpower[initrad];
            /*         radpower for precomputation */

        }

        public NqData data;
        

        /* Initialise network in range (0,0,0) to (255,255,255) and set parameters
        ----------------------------------------------------------------------- */

        public NeuQuant(byte[] thepic, int len, int sample)
        {
            fixed (int* network = data.network)
            fixed (int* freq = data.freq)
            fixed (int* bias = data.bias)
            {
                int i;

                thepicture = thepic;
                lengthcount = len;
                samplefac = sample;

                for (i = 0; i < netsize; i++)
                {
                    network[i * 4 + 0] = network[i * 4 + 1] = network[i * 4 + 2] = (i << (netbiasshift + 8)) / netsize;
                    freq[i] = intbias / netsize; /*                 1/netsize */
                    bias[i] = 0;
                }
            }
        }

        public byte[] ColorMap()
        {
            byte[] bmap = new byte[3 * netsize];
            int[] bindex = new int[netsize];
            fixed (int* network = data.network)
            fixed (int* index = bindex)
            fixed (byte* map = bmap)
            {
                for (int i = 0; i < netsize; i++)
                    index[network[i*4 + 3]] = i;
                int k = 0;
                for (int i = 0; i < netsize; i++)
                {
                    int j = index[i];
                    map[k++] = (byte) (network[j*4 + 0]);
                    map[k++] = (byte) (network[j*4 + 1]);
                    map[k++] = (byte) (network[j*4 + 2]);
                }
            }
            return bmap;
        }

        /*         Insertion sort of network and building of netindex[0..255] (to do after unbias)
           ------------------------------------------------------------------------------- */

        public void Inxbuild()
        {
            fixed (int* network = data.network)
            fixed (int* netindex = data.netindex)
            {
                int i, j, smallpos, smallval;
                int[] p;
                int[] q;
                int previouscol, startpos;

                previouscol = 0;
                startpos = 0;
                for (i = 0; i < netsize; i++)
                {
                    smallpos = i;
                    smallval = network[i*4 + 1]; /*                 index on g */
                    /* find smallest in i..netsize-1 */
                    for (j = i + 1; j < netsize; j++)
                    {
                        if (network[j*4 + 1] < smallval)
                        {
                            /*                     index on g */
                            smallpos = j;
                            smallval = network[j*4 + 1]; /*                         index on g */
                        }
                    }
                    /*                 swap p (i) and q (smallpos) entries */
                    if (i != smallpos)
                    {
                        j = network[smallpos*4 + 0];
                        network[smallpos*4 + 0] = network[i*4 + 0];
                        network[i*4 + 0] = j;
                        j = network[smallpos*4 + 1];
                        network[smallpos*4 + 1] = network[i*4 + 1];
                        network[i*4 + 1] = j;
                        j = network[smallpos*4 + 2];
                        network[smallpos*4 + 2] = network[i*4 + 2];
                        network[i*4 + 2] = j;
                        j = network[smallpos*4 + 3];
                        network[smallpos*4 + 3] = network[i*4 + 3];
                        network[i*4 + 3] = j;
                    }
                    /*                 smallval entry is now in position i */
                    if (smallval != previouscol)
                    {
                        netindex[previouscol] = (startpos + i) >> 1;
                        for (j = previouscol + 1; j < smallval; j++)
                            netindex[j] = i;
                        previouscol = smallval;
                        startpos = i;
                    }
                }
                netindex[previouscol] = (startpos + maxnetpos) >> 1;
                for (j = previouscol + 1; j < 256; j++)
                    netindex[j] = maxnetpos; /*                 really 256 */
            }
        }

        /*         Main Learning Loop
           ------------------ */

        public unsafe void Learn()
        {
            fixed (int* network = data.network)
            fixed (int* radpower = data.radpower)
            fixed (int* bias = data.bias)
            fixed (int* freq = data.freq)
            {
                int i, j, b, g, r;
                int radius, rad, alpha, step, delta, samplepixels;
                int pix, lim;

                if (lengthcount < minpicturebytes)
                    samplefac = 1;
                alphadec = 30 + ((samplefac - 1)/3);
                fixed (byte* p = thepicture)
                {
                    pix = 0;
                    lim = lengthcount;
                    samplepixels = lengthcount/(3*samplefac);
                    delta = samplepixels/ncycles;
                    alpha = initalpha;
                    radius = initradius;

                    rad = radius >> radiusbiasshift;
                    if (rad <= 1)
                        rad = 0;
                    for (i = 0; i < rad; i++)
                        radpower[i] =
                            alpha*(((rad*rad - i*i)*radbias)/(rad*rad));

                    //fprintf(stderr,"beginning 1D learning: initial radius=%d\n", rad);

                    if (lengthcount < minpicturebytes)
                        step = 3;
                    else if ((lengthcount%prime1) != 0)
                        step = 3*prime1;
                    else
                    {
                        if ((lengthcount%prime2) != 0)
                            step = 3*prime2;
                        else
                        {
                            if ((lengthcount%prime3) != 0)
                                step = 3*prime3;
                            else
                                step = 3*prime4;
                        }
                    }

                    i = 0;
                    while (i < samplepixels)
                    {
                        b = (p[pix + 0] & 0xff) << netbiasshift;
                        g = (p[pix + 1] & 0xff) << netbiasshift;
                        r = (p[pix + 2] & 0xff) << netbiasshift;
                        j = Contest(network, bias, freq, b, g, r);

                        Altersingle(network, alpha, j, b, g, r);
                        if (rad != 0)
                            Alterneigh(network, radpower, rad, j, b, g, r); /*                     alter neighbours */

                        pix += step;
                        if (pix >= lim)
                            pix -= lengthcount;

                        i++;
                        if (delta == 0)
                            delta = 1;
                        if (i%delta == 0)
                        {
                            alpha -= alpha/alphadec;
                            radius -= radius/radiusdec;
                            rad = radius >> radiusbiasshift;
                            if (rad <= 1)
                                rad = 0;
                            for (j = 0; j < rad; j++)
                                radpower[j] =
                                    alpha*(((rad*rad - j*j)*radbias)/(rad*rad));
                        }
                    }
                    //fprintf(stderr,"finished 1D learning: readonly alpha=%f !\n",((float)alpha)/initalpha);
                }
            }
        }

        /*         Search for BGR values 0..255 (after net is unbiased) and return colour index
           ---------------------------------------------------------------------------- */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Map(int* network, int* netindex, int b, int g, int r)
        {
            int i, j, dist, a, bestd;
            int[] p;
            int best;

            bestd = 1000; /*             biggest possible dist is 256*3 */
            best = -1;
            i = netindex[g]; /*             index on g */
            j = i - 1; /*             start at netindex[g] and work outwards */

            while ((i < netsize) || (j >= 0))
            {
                if (i < netsize)
                {
                    dist = network[i * 4 + 1] - g; /*                     inx key */
                    if (dist >= bestd)
                        i = netsize; /*                         stop iter */
                    else
                    {
                        i++;
                        if (dist < 0)
                            dist = -dist;
                        a = network[i * 4 + 0] - b;
                        if (a < 0)
                            a = -a;
                        dist += a;
                        if (dist < bestd)
                        {
                            a = network[i * 4 + 2] - r;
                            if (a < 0)
                                a = -a;
                            dist += a;
                            if (dist < bestd)
                            {
                                bestd = dist;
                                best = network[i * 4 + 3];
                            }
                        }
                    }
                }
                if (j >= 0)
                {
                    dist = g - network[j * 4 + 1]; /*                     inx key - reverse dif */
                    if (dist >= bestd)
                        j = -1; /*                         stop iter */
                    else
                    {
                        j--;
                        if (dist < 0)
                            dist = -dist;
                        a = network[j * 4 + 0] - b;
                        if (a < 0)
                            a = -a;
                        dist += a;
                        if (dist < bestd)
                        {
                            a = network[j * 4 + 2] - r;
                            if (a < 0)
                                a = -a;
                            dist += a;
                            if (dist < bestd)
                            {
                                bestd = dist;
                                best = network[j * 4 + 3];
                            }
                        }
                    }
                }
            }
            return (best);
        }

        public byte[] Process()
        {
            Learn();
            Unbiasnet();
            Inxbuild();
            return ColorMap();
        }

        /*         Unbias network to give byte values 0..255 and record position i to prepare for sort
           ----------------------------------------------------------------------------------- */

        public void Unbiasnet()
        {
            fixed (int* network = data.network)
            {
                int i, j;

                for (i = 0; i < netsize; i++)
                {
                    network[i*4 + 0] >>= netbiasshift;
                    network[i*4 + 1] >>= netbiasshift;
                    network[i*4 + 2] >>= netbiasshift;
                    network[i*4 + 3] = i; /*                 record colour no */
                }
            }
        }

        /*         Move adjacent neurons by precomputed alpha*(1-((i-j)^2/[r]^2)) in radpower[|i-j|]
           --------------------------------------------------------------------------------- */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void Alterneigh(int* network, int* radpower, int rad, int i, int b, int g, int r)
        {
            int j, k, lo, hi, a, m;
            int[] p;

            lo = i - rad;
            if (lo < -1)
                lo = -1;
            hi = i + rad;
            if (hi > netsize)
                hi = netsize;

            j = i + 1;
            k = i - 1;
            m = 1;
            while ((j < hi) || (k > lo))
            {
                a = radpower[m++];
                if (j < hi)
                {
                    try
                    {
                        network[j * 4 + 0] -= (a * (network[j * 4 + 0] - b)) / alpharadbias;
                        network[j * 4 + 1] -= (a * (network[j * 4 + 1] - g)) / alpharadbias;
                        network[j * 4 + 2] -= (a * (network[j * 4 + 2] - r)) / alpharadbias;
                    }
                    catch (Exception e)
                    {
                    } // prevents 1.3 miscompilation
                    ++j;
                }
                if (k > lo)
                {
                    try
                    {
                        network[k * 4 + 0] -= (a * (network[k * 4 + 0] - b)) / alpharadbias;
                        network[k * 4 + 1] -= (a * (network[k * 4 + 1] - g)) / alpharadbias;
                        network[k * 4 + 2] -= (a * (network[k * 4 + 2] - r)) / alpharadbias;
                    }
                    catch (Exception e)
                    {
                    }
                    --k;
                }
            }
        }

        /*         Move neuron i towards biased (b,g,r) by factor alpha
           ---------------------------------------------------- */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void Altersingle(int* network, int alpha, int i, int b, int g, int r)
        {
            /*             alter hit neuron */
            network[i * 4 + 0] -= (alpha * (network[i * 4 + 0] - b)) / initalpha;
            network[i * 4 + 1] -= (alpha * (network[i * 4 + 1] - g)) / initalpha;
            network[i * 4 + 2] -= (alpha * (network[i * 4 + 2] - r)) / initalpha;
        }

        /*         Search for biased BGR values
           ---------------------------- */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected int Contest(int* network, int* bias, int* freq, int b, int g, int r)
        {
            /*             finds closest neuron (min dist) and updates freq */
            /* finds best neuron (min dist-bias) and returns position */
            /*             for frequently chosen neurons, freq[i] is high and bias[i] is negative */
            /* bias[i] = gamma*((1/netsize)-freq[i]) */

            int i, dist, a, biasdist, betafreq;
            int bestpos, bestbiaspos, bestd, bestbiasd;
            int[] n;

            bestd = ~(((int)1) << 31);
            bestbiasd = bestd;
            bestpos = -1;
            bestbiaspos = bestpos;

            for (i = 0; i < netsize; i++)
            {
                dist = network[i * 4 + 0] - b;
                if (dist < 0)
                    dist = -dist;
                a = network[i * 4 + 1] - g;
                if (a < 0)
                    a = -a;
                dist += a;
                a = network[i * 4 + 2] - r;
                if (a < 0)
                    a = -a;
                dist += a;
                if (dist < bestd)
                {
                    bestd = dist;
                    bestpos = i;
                }
                biasdist = dist - ((bias[i]) >> (intbiasshift - netbiasshift));
                if (biasdist < bestbiasd)
                {
                    bestbiasd = biasdist;
                    bestbiaspos = i;
                }
                betafreq = (freq[i] >> betashift);
                freq[i] -= betafreq;
                bias[i] += (betafreq << gammashift);
            }
            freq[bestpos] += beta;
            bias[bestpos] -= betagamma;
            return (bestbiaspos);
        }
    }
}
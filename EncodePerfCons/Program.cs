﻿using System;

namespace EncodePerfCons
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine ("Hello World!");
            Console.WriteLine("quick pre");
            Gif.Components.Timer.TimeIt(128, 128, 1);
            Gif.Components2.Timer.TimeIt(128, 128, 1);
            var t = Gif.Components.Timer.TimeIt(540, 540, 10);
            Console.WriteLine("untuned {0} ms", t);
            var t2 = Gif.Components.Timer.TimeIt(540, 540, 10);
            Console.WriteLine("tuned {0} ms", t2);
        }
    }
}
